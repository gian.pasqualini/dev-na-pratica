namespace SeniorSistemas.Mydomain.Myservice
{
    
    /// This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
    
    /// <summary> Handler interface for helloWorld.</summary>
	[System.CodeDom.Compiler.GeneratedCodeAttribute("sdl", "22.4.0")]
    public interface IHelloWorld
    {
    	
        ///<summary>
        /// TBD
        ///</summary>
        HelloWorldOutput HelloWorld(HelloWorldInput request);
        
    }
}
