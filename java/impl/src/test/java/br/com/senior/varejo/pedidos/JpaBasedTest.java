package br.com.senior.varejo.pedidos;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.inject.Inject;

import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;

import br.com.senior.messaging.model.FakeServiceContext;
import br.com.senior.messaging.model.SpringContext;
import br.com.senior.messaging.model.db.PersistenceManager;
import br.com.senior.messaging.model.db.migration.MigrationManager;


@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles(SpringContext.UNIT_TEST_PROFILE)
public class JpaBasedTest {
    
    private static String SCHEMA_NAME = "TEST";
    private static String TENANT_NAME = "TEST";

    @Inject
    protected PersistenceManager persistence;

    @Inject
    protected MigrationManager migration;

    @Configuration
    @ComponentScan({ "br.com.senior" })
    public static class TestConfig {
    }

    @BeforeClass
    public static void prepare() {
        FakeServiceContext.setUp(TENANT_NAME);
    }

    @Before
    public void setUp() throws SQLException, IOException {
        try (Connection conn = persistence.getDataSource().getConnection()) {
            resetBase(conn);

            String scriptFile = "scripts/" + this.getClass().getSimpleName() + ".sql";
            InputStream scriptResource = ClassLoader.getSystemResourceAsStream(scriptFile);
            if (scriptResource != null) {
                try (InputStreamReader isr = new InputStreamReader(scriptResource)) {
                    System.out.println("Running script " + scriptFile);
                    RunScript.execute(conn, isr);
                }
            }
        }
    }

    private void resetBase(Connection conn) throws SQLException {
        try (Statement statement = conn.createStatement()) {
            statement.execute("DROP ALL OBJECTS");
            statement.execute("CREATE SCHEMA " + SCHEMA_NAME);
            statement.execute("SET SCHEMA " + SCHEMA_NAME);
        }
        migration.reset();
        migration.execute();
    }

    protected void runScript(String scriptFile) throws SQLException, IOException {
        try (Connection conn = persistence.getDataSource().getConnection()) {
            resetBase(conn);

            InputStream scriptResource = ClassLoader.getSystemResourceAsStream(scriptFile);
            if (scriptResource != null) {
                try (InputStreamReader isr = new InputStreamReader(scriptResource)) {
                    System.out.println("Running script " + scriptFile);
                    RunScript.execute(conn, isr);
                }
            }
        }
    }


}
