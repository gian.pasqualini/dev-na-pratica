package br.com.senior.varejo.pedidos;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import br.com.senior.messaging.model.SpringContext;
import br.com.senior.messaging.model.db.DatabaseConfiguration;

@Component
@Profile(SpringContext.UNIT_TEST_PROFILE)
public class H2DatabaseConfiguration implements DatabaseConfiguration{

    @Override
    public String getUrl() {
        return "jdbc:h2:mem:notification-test;DB_CLOSE_DELAY=-1;INIT=CREATE SCHEMA IF NOT EXISTS TEST\\;SET SCHEMA TEST";
    }

    @Override
    public String getUsername() {
        return "sa";
    }

    @Override
    public String getPassword() {
        return "sa";
    }

    @Override
    public String getSchemaName() {
        return "TEST";
    }

    
}
