package br.com.senior.varejo.pedidos;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import java.time.LocalDate;

import br.com.senior.messaging.model.SpringContext;
import br.com.senior.varejo.pedidos.JpaBasedTest.TestConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
@ActiveProfiles({ SpringContext.UNIT_TEST_PROFILE })
public class ClientesTest extends JpaBasedTest {
    
    private static final String CLINT_EASTWOOD = "Clint Eastwood";
    private static final String NAME = "John Travolta";
    @Autowired
    private ClienteCrudService clienteService;

    @Test
    public void leClientesDaBase() {
        Page<ClienteEntity> page = clienteService.listClientePageable(0, 1);
        ClienteEntity clienteEntity = page.getContent().get(0);

        assertEquals(NAME , clienteEntity.getNome());

    }
    
    @Test
    public void removeClientesDaBase() {
        Page<ClienteEntity> page = clienteService.listClientePageable(0, 1);
        ClienteEntity clienteEntity = page.getContent().get(0);
        
        clienteService.deleteCliente(new Cliente.Id(clienteEntity.getId().toString()));
        
        page = clienteService.listClientePageable(0, 1);
        
        assertEquals(0 , page.getContent().size());

    }

    @Test
    public void updateClientesDaBase() {
        Page<ClienteEntity> page = clienteService.listClientePageable(0, 1);
        ClienteEntity clienteEntity = page.getContent().get(0);
        
        clienteEntity.setNome(CLINT_EASTWOOD);
        
        clienteService.updateCliente(clienteEntity);
        
        ClienteEntity clienteAlterado = clienteService.retrieveCliente(new Cliente.Id(clienteEntity.getId().toString()));

        assertEquals(CLINT_EASTWOOD , clienteAlterado.getNome());

    }
    
    
    @Test
    public void criaClientesDaBase() {
        
        ClienteEntity cliente = new ClienteEntity();
        cliente.setCpf("00011100088");
        cliente.setNome(CLINT_EASTWOOD);
        cliente.setCreditoHabilitado(true);
        cliente.setDataNascimento(LocalDate.now());

        ClienteEntity create = (ClienteEntity) clienteService.create(cliente);

        ClienteEntity clienteInserido = clienteService.retrieveCliente(new Cliente.Id(create.getId().toString()));
        
        assertNotNull(clienteInserido);

    }

}
