package br.com.senior.messaging.model;

import java.util.Arrays;

import br.com.senior.messaging.Message;
import br.com.senior.varejo.pedidos.PedidosConstants;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class FakeServiceContext {
    
    private static ServiceRunner mockServiceRunner;

    public static void setUp(String tenant) {
        mockServiceRunner = mock(ServiceRunner.class);

        mockServiceRunner.service = mock(Service.class);
        when(mockServiceRunner.getService()).thenReturn(mockServiceRunner.service);
        when(mockServiceRunner.service.getDomain()).thenReturn(PedidosConstants.DOMAIN);
        when(mockServiceRunner.service.getName()).thenReturn(PedidosConstants.SERVICE);

        Message mockMessage = mock(Message.class);
        when(mockMessage.getSelector()).thenReturn(tenant);

        when(mockMessage.containsHeader(Message.USERNAME_HEADER)).thenReturn(true);
        when(mockMessage.containsHeader(Message.SELECTOR_HEADER)).thenReturn(true);
        when(mockMessage.getUsername()).thenReturn("admin");

        when(mockMessage.followUp(any(), any(), any(), any())).thenAnswer(invocation -> {
            Message message = mock(Message.class);
            when(message.getSelector()).thenReturn(tenant);
            when(message.getPrimitive()).thenReturn(invocation.getArgument(2));
            when(message.getContent()).thenReturn(invocation.getArgument(3));
            return message;
        });

        ServiceContext.install(new ServiceContext(Arrays.asList(mockServiceRunner.service), (Service service) -> mockServiceRunner));
        ServiceContext.get().setCurrentMessage(mockServiceRunner, mockMessage);
    }

    public static ServiceRunner getMockServiceRunner() {
        return mockServiceRunner;
    }


}
